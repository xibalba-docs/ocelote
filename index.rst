.. title:: Ocelote, utilerías y azúcar sintáctico de propósito general

========================
Documentación de Ocelote
========================

`Ocelote <https://gitlab.com/xibalba/ocelote>`_ es un paquete PHP de utiliterías, azúcar sintáctico y envolventes OO para funciones de uso común.

.. code-block:: php

	// Verificar si una variable está vacía
	if(Checker::isEmpty($variable)) {
		// Hacer algo...
	}

	// Convertir una cadena json en un arreglo
	$data = Converter::toArray($someJson);

	// Explotar una cadena
	$strAsArray = StringHelper::explode($someString, ",");

¡Y mucho más!

.. toctree::
	:maxdepth: 4

	pages/00_intro
